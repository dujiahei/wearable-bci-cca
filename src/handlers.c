/*
 * handlers.c
 *
 *  Created on: Nov 21, 2017
 *      Author: Micrel
 */
#define USE_DMA 0

#include "handlers.h"
#include "ads1298_related_functions.h"
//#include "profileSTM.h"

void Handle_DMA_TF();
extern void USART_Prepare_Packet(void);
extern int TX_ready;

void DMA1_Stream3_IRQHandler(void)
{
	/* Test on DMA Stream Transfer Complete interrupt */
	if(DMA_GetITStatus(DMA1_Stream3, DMA_IT_TCIF3))
	{
		/* Clear DMA Stream Transfer Complete interrupt pending bit */
		DMA_ClearITPendingBit(DMA1_Stream3, DMA_IT_TCIF3);

		/* Turn LED3 on: End of Transfer */
		//STM_EVAL_LEDOn(LED6);
		SPI_RX_GOING = 0;
		Handle_DMA_TF();
//		GPIO_ToggleBits( GPIOA, GPIO_Pin_8);


	}

}

extern int dma_working;
void Handle_DMA_TF()
{
	static int donwsample_counter = DOWNSAMPLE_FACTOR_STREAM - 1;
	static int signal_downsample_counter = DS - 1;
	if(MAIN_FSM_state == MAIN_FSM_STREAM)
	{
		if(SPI_RX_GOING == 0)         //Configure indexes and flags
		{
			SPI_RX_GOING = 1;
			SPI_TxIndex = 0;
			SPI_RxIndex = 0;
			TX_ready = 1;
		}


		if(TX_ready)
		{
			TX_ready = 0;									//reset reception flag, the interrupt will activate it when data is received.
			//if(++signal_downsample_counter == DOWNSAMPLE_FACTOR)
			{
				signal_downsample_counter = 0;
				USART_Prepare_Packet();						//prepare packet to send.
			}
			dma_working = 0;
			//GPIO_ToggleBits( GPIOA, GPIO_Pin_9);			//toggle led as indicator.

			//USART_Config();

			if(++donwsample_counter == DOWNSAMPLE_FACTOR_STREAM)
			{
				donwsample_counter = 0;

				while(CommTxBufferIndex < current_TX_buffer_length)
				{
					USART_SendData(USART2, CommTxBuffer[CommTxBufferIndex++]);
					while (USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET){}
				}

				GPIO_ToggleBits( GPIOA, GPIO_Pin_9);
				CommTxBufferIndex = 0;
			}
		}
	}
}



  void DMA2_Stream0_IRQHandler() {
  }

  void EXTI9_5_IRQHandler(void)
  {
#ifdef USE_DMA
	  if ((EXTI_GetITStatus(EXTI_Line7) == SET))
	      {
	        /* Clear the EXTI line 1 pending bit */
	        EXTI_ClearITPendingBit(EXTI_Line7);
	        GPIO_ToggleBits( GPIOA, GPIO_Pin_8);  //LED BLINK

	        ADS_Read_DMA();


	      }

#else

    if ((EXTI_GetITStatus(EXTI_Line7) == SET))
    {
      /* Clear the EXTI line 1 pending bit */
      EXTI_ClearITPendingBit(EXTI_Line7);
      GPIO_ToggleBits( GPIOA, GPIO_Pin_8);  //LED BLINK

      SPI_I2S_ReceiveData(SPI2);
      SPI_I2S_ITConfig(SPI2, SPI_I2S_IT_RXNE, ENABLE);
  	 SPI_I2S_ITConfig(SPI2, SPI_I2S_IT_TXE, ENABLE);

    }
#endif
  }

  void USART2_IRQHandler(void)
  {

  // Interrupt for reception
  	if(USART_GetITStatus(USART2, USART_IT_RXNE) == SET)
  	{
  		char rec;

  		rec = USART_ReceiveData(USART2);
  		switch(rec)
  		 {
  		 	case(START_STREAM_CMD):
  		 		stream_enable = 1;
  		 	break;

  		 	case(STOP_STREAM_CMD):
  		 		stream_enable = 0;
  		 	break;

  		 	case(PACKET_REQUESTED):
				//send_packet = 1;
  		 	break;

  		 	default:
  		 		//stream_enable = 0;
  		 	break;



  		 }




  	}

  	// Interrupt for transmission
  	if(USART_GetITStatus(USART2, USART_IT_TXE) != RESET)
  	{


  		if(CommTxBufferIndex < current_TX_buffer_length)
  			//Send next char present in the buffer
  			USART_SendData(USART2, CommTxBuffer[CommTxBufferIndex++]);
  		else
  		{
  			//If all data in tx buffer have been transmtted, disable tx buffer
  			USART_ITConfig(USART2,USART_IT_TXE,DISABLE);
  			GPIO_ToggleBits( GPIOA, GPIO_Pin_9);
  			CommTxBufferIndex = 0;
  		}
  	}


  }

  void SysTick_Handler(void)
  {
  	static int tic = 0 ;
  	static int tic_config_100ms = 0;
  	static int tic_config_500ms = 0;

  	//added
  	static int tic_config_10ms = 0;
  	//end added

  	if(tic++ > 500)
  	{
  		tic = 0;
  		//TX_ready = 1;
  		//GPIO_ToggleBits( GPIOA, GPIO_Pin_9);

  	}

  	en_timeou++;
  	if(tic_config_100ms++ >=100)
  	{
  		tic_config_100ms = 0;
  		ms100_flag = 1;
  	}

  	if(tic_config_500ms++ >=500)
  	{
  		tic_config_500ms = 0;
  		ms500_flag = 1;
  	}

  	if(tic_config_10ms++ >=50)
  	{
  		tic_config_10ms = 0;
  		ms10_flag = 1;
  	}



  }



