/*
 * stm_related_functions.c
 *
 *  Created on: Nov 21, 2017
 *      Author: Micrel
 */


#include "stm_related_functions.h"

void GPIO_Config_LEDS(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Enable the GPIO_LED Clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	/* Configure the GPIO_LED pin 8 */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Configure the GPIO_LED pin 9 */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

}

void GPIO_Config_REGULATOR_DRIVERS(void)
{
 	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
 	GPIO_InitTypeDef GPIO_InitStructure;


    /* Configure the BT REGULATOR PIN */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

    /* Configure the IMU REGULATOR PIN */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

}


void SPI_NVIC_Config(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;

  /* 1 bit for pre-emption priority, 3 bits for subpriority */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

  /* Configure and enable SPI_MASTER interrupt -------------------------------*/
  NVIC_InitStructure.NVIC_IRQChannel = SPI2_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

void SPI_DRDY_EXTI_Config(void)
{
  EXTI_InitTypeDef   EXTI_InitStructure;
  GPIO_InitTypeDef   GPIO_InitStructure;
  NVIC_InitTypeDef   NVIC_InitStructure;

  /* Enable GPIOA clock */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

  GPIO_Init(GPIOA, &GPIO_InitStructure);


/*	GPIO_InitStructure.GPIO_Pin = DIF_DAT_PIN;
  GPIO_Init(DIF_DAT_PORT, &GPIO_InitStructure);


	GPIO_InitStructure.GPIO_Pin = DIF_CLK_PIN;
  GPIO_Init(DIF_CLK_PORT, &GPIO_InitStructure);*/

  /* Enable SYSCFG clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

  /* Connect EXTI7 Line to PA7 pin */  //Interrupt del DRDY
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource7);

  /* Configure EXTI7 line */
  EXTI_InitStructure.EXTI_Line = EXTI_Line7;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Enable and set EXTI7 Interrupt to the lowest priority */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}


void USART_Config(void)
{
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Enable UART clock */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	/* Connect PD5 to USART2_Tx*/
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource5, GPIO_AF_USART2);

	/* Connect PD6 to USART2_Rx*/
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource6, GPIO_AF_USART2);

	/* Configure USART Tx and Rx as alternate function  */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_Init(GPIOD, &GPIO_InitStructure);

	/* USARTx configured as follows:
		- BaudRate = 921600 baud
		- Word Length = 8 Bits
		- One Stop Bit
		- No parity
		- Hardware flow control disabled (RTS and CTS signals)
		- Receive and transmit enabled
	*/
	//USART_InitStructure.USART_BaudRate = 460800;
    //921600, 115200
	USART_InitStructure.USART_BaudRate = 921600;  //change to  initialize the BT node...
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	/* USART initialization */
	USART_Init(USART2, &USART_InitStructure);

	/* Enable the USARTy Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* Enable USART */
	USART_Cmd(USART2, ENABLE);
}






extern int tics_sampling;
extern int new_window;

void USART_Prepare_Packet(void)
{
	static int count_buffer=0;
		tics_sampling++;
	   //headers
	   pkt_idx = 0;

	   /*
	   CommTxBuffer[pkt_idx++] = START;
	   CommTxBuffer[pkt_idx++] = PKT_ID;
	   CommTxBuffer[pkt_idx++] = (uint8_t)packetcounter++;
	    */

	   //PCK counter
	   CommTxBuffer[pkt_idx++] = (uint8_t)packetcounter++;

	   for(int i = 0; i < N_CHANNELS; i++)
	   {
		   //Send Channel i//
		   valore = 0;
		   valore += (u32)SPI_RxBuffer[3+3*i]    <<  24       & 0xFF000000;
		   valore += (u32)SPI_RxBuffer[4+3*i]    << (24 - 8)  & 0x00FF0000;
		   valore += (u32)SPI_RxBuffer[5+3*i]    << (24 - 16) & 0x0000FF00;
		   valore = two2dec(valore);///256;

		   ADDdataM[i][count_buffer] = valore;

		   //send chi
			//CommTxBuffer[pkt_idx++] = valore>>8;
			CommTxBuffer[pkt_idx++] = valore>>16;
			CommTxBuffer[pkt_idx++] = valore>>24;
	   }

	   //Necessary for Android app
	   for(int i = N_CHANNELS; i < MAX_N_CHANNELS; i++)
	   {
			//CommTxBuffer[pkt_idx++] = 69;
			CommTxBuffer[pkt_idx++] = 69;
			CommTxBuffer[pkt_idx++] = 69;
	   }

	   for(int i = 0; i < N_FREQs; i++) CommTxBuffer[pkt_idx++] = (int32_t) (correlations[i]*MF)>>0;
	   //Necessary for Android app
	   for(int i = N_FREQs; i < MAX_N_FREQs; i++) CommTxBuffer[pkt_idx++] = 0;

//		//SVM output
//		 CommTxBuffer[pkt_idx++] = (int32_t) (SVM_output)>>0;
//
//		// Three aux channels
//		CommTxBuffer[pkt_idx++] = 69;
//		CommTxBuffer[pkt_idx++] = 69;
//		CommTxBuffer[pkt_idx++] = 69;

		current_TX_buffer_length = pkt_idx+1;


//	   //Send Channel 1//
//	   valore = 0;
//	   valore += (u32)SPI_RxBuffer[3]    <<  24       & 0xFF000000;
//	   valore += (u32)SPI_RxBuffer[4]    << (24 - 8)  & 0x00FF0000;
//	   valore += (u32)SPI_RxBuffer[5]    << (24 - 16) & 0x0000FF00;
//	   valore = two2dec(valore)/256;
//
//	   ADDdataM[0][count_buffer] = valore;
//
//	   //send Channel 2//
//	    valore = 0;
//	    valore += (u32)SPI_RxBuffer[6]    <<  24       & 0xFF000000;
//	    valore += (u32)SPI_RxBuffer[7]    << (24 - 8)  & 0x00FF0000;
//	    valore += (u32)SPI_RxBuffer[8]    << (24 - 16) & 0x0000FF00;
//	    valore = two2dec(valore)/256;
//
//	    ADDdataM[1][count_buffer] = valore;
//
//		//send Channel 3//
//	    valore = 0;
//	    valore += (u32)SPI_RxBuffer[9]    <<  24       & 0xFF000000;
//	    valore += (u32)SPI_RxBuffer[10]    << (24 - 8)  & 0x00FF0000;
//	    valore += (u32)SPI_RxBuffer[11]    << (24 - 16) & 0x0000FF00;
//	    valore = two2dec(valore)/256;
//
//	    ADDdataM[2][count_buffer] = valore;
//
//	    //PCK counter
//	    CommTxBuffer[pkt_idx++] = (uint8_t)packetcounter++;
//
//	    //send ch1
//	    CommTxBuffer[pkt_idx++] = ADDdataM[0][count_buffer]>>8;
//	    CommTxBuffer[pkt_idx++] = ADDdataM[0][count_buffer]>>16;
//	    CommTxBuffer[pkt_idx++] = ADDdataM[0][count_buffer]>>24;
//
//	    //send ch2
//	    CommTxBuffer[pkt_idx++] = ADDdataM[1][count_buffer]>>8;
//	    CommTxBuffer[pkt_idx++] = ADDdataM[1][count_buffer]>>16;
//	    CommTxBuffer[pkt_idx++] = ADDdataM[1][count_buffer]>>24;
//
//	    //send ch3
//	    CommTxBuffer[pkt_idx++] = ADDdataM[2][count_buffer]>>8;
//	    CommTxBuffer[pkt_idx++] = ADDdataM[2][count_buffer]>>16;
//	    CommTxBuffer[pkt_idx++] = ADDdataM[2][count_buffer]>>24;
//
//	    //correlations
//	    CommTxBuffer[pkt_idx++] = (int32_t) (correlations[0]*MF)>>0;
//	    CommTxBuffer[pkt_idx++] = (int32_t) (correlations[1]*MF)>>0;
//	    CommTxBuffer[pkt_idx++] = (int32_t) (correlations[2]*MF)>>0;
//	    CommTxBuffer[pkt_idx++] = (int32_t) (correlations[3]*MF)>>0;
//	    CommTxBuffer[pkt_idx++] = (int32_t) (correlations[4]*MF)>>0;
//	    CommTxBuffer[pkt_idx++] = (int32_t) (correlations[5]*MF)>>0;
//	    CommTxBuffer[pkt_idx++] = (int32_t) (correlations[6]*MF)>>0;
//	    CommTxBuffer[pkt_idx++] = (int32_t) (correlations[7]*MF)>>0;
//	    CommTxBuffer[pkt_idx++] = 69;
//	    CommTxBuffer[pkt_idx++] = 69;
//	    CommTxBuffer[pkt_idx++] = 69;
//	    CommTxBuffer[pkt_idx++] = 69;
//	    CommTxBuffer[pkt_idx++] = 69;
//	    CommTxBuffer[pkt_idx++] = 69;
//	    CommTxBuffer[pkt_idx++] = 69;
//	    CommTxBuffer[pkt_idx++] = 69;
//
//	    //SVM output
//	    CommTxBuffer[pkt_idx++] = (int32_t) (SVM_output)>>0;
//
//	    //trigger value
//	    //CommTxBuffer[pkt_idx++] = (int32_t) (trigger)>>0;
//
//	    //CommTxBuffer[pkt_idx] =  GetCheckSum(CommTxBuffer, pkt_idx);
//	    current_TX_buffer_length = pkt_idx + 1;

	    if (++count_buffer >= SHIFT_WINDOW_SIZE)
		{
	    	count_buffer=0;
/*
	    	if(new_window ==1)
	    		while(1)
	    			{GPIO_ToggleBits( GPIOA, GPIO_Pin_9); for(int i = 0; i<6000000; i++);		 }

	    	new_window = 1;
	*/
	    	new_window = 1;
	    	//new_window_available = count_buffer;
	    	//micro_window_counter=0;

    		//dowhatyouhavetodoCCA(new_window_available);

		}

}


int32_t two2dec(uint32_t val){					// 2-complement from the received data to recover sign from the data
   u8 c = 0;									// received from the ADS through SPI
   int32_t ADD;
   c = val >> 31;

            if (c == 0)
            {
               ADD = val;
            }

            else {
               val= ~val;
               val = val + 1;
               ADD = - val;
            }

      return ADD;

}

char GetCheckSum(char*buffer, int n)						//CheckSum for the transmitted data.
{
   char crc = 0;
   int i;

   for(i = 0; i < n; i++)
      crc = crc ^ buffer[i];

   return crc;
}





