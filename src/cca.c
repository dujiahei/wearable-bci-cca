/*
 * cca.c
 *
 *  Created on: 15 Nov 2017
 *      Author: matti
 */

#include "dma.h"
#include "cca.h"

/*********** Includes  ****************/
#include "stm32f4xx.h"
#include "stm32f4_discovery.h"

#include <math.h>

#include "svd.h"
#include "mat_ops.h"
#include "profileSTM.h"
#include "arm_math.h"
#include "defines_cca.h"


extern float32_t QYg[N_FREQs][2*N_HARMONICS][EV_WINDOWS_SIZE];

extern DMA_Stream_TypeDef* streams[2];


/**
 * n_samples: number of samples in the window
 * n_channels: number of channels processed
 * n_ref_signals: number of reference signals (sin and cos of each harmonic)
 * freq_index: the index of the frequency the input is tested for
 * X_real_ptr: pointer to the allocated input matrix
 *
 * returns: the norm of the first D cca values, where D = min(n_channels, n_ref_signals)
 *
 */


#pragma GCC push_options
//#pragma GCC optimize ("O2")
#pragma GCC optimize ("O0")


float32_t cca(int freq_index, float32_t QX[][EV_WINDOWS_SIZE])  {

	int d = N_CHANNELS;	// ASSUMING WE USE AT LEAST TWO HARMONICS
	float32_t d_values[d];

	if (N_CHANNELS>=N_REF_SIGNALS) {
		float32_t QXtQY[N_CHANNELS][N_REF_SIGNALS];
		float32_t v[N_CHANNELS][N_REF_SIGNALS];
		// Multiplication: QX times QY transposed (constant defined in QYg[index]).
		// Transpose the result.
		mat_mult_t1_tres(ACQ_WINDOW_SIZE, N_CHANNELS, QX, ACQ_WINDOW_SIZE, N_REF_SIGNALS, QYg[freq_index], QXtQY);
		// SVD
		dsvd(N_CHANNELS, N_REF_SIGNALS, QXtQY, d_values, v);
	}
	else {
		float32_t QXtQY[N_REF_SIGNALS][N_CHANNELS];
		float32_t v[N_REF_SIGNALS][N_CHANNELS];
		// Multiplication: QX transposed times QY (constant defined in QYg[index]).
		mat_mult_t1(EV_WINDOWS_SIZE, N_CHANNELS, QX, EV_WINDOWS_SIZE, N_REF_SIGNALS, QYg[freq_index], QXtQY);
		// SVD
		dsvd(N_REF_SIGNALS, N_CHANNELS, QXtQY, d_values, v);
	}

	float32_t n = norm(d_values,0,d);

	return n;

}


void qr_dec(float32_t X[N_CHANNELS][EV_WINDOWS_SIZE], float32_t Q[N_CHANNELS][EV_WINDOWS_SIZE]) {

	float32_t d[N_CHANNELS];
	qrhous(X, d);

	for (int col_index=0; col_index<N_CHANNELS; col_index++) {
		float32_t i[EV_WINDOWS_SIZE]={0}; // Column of diagonal eye matrix
		i[col_index]=1;
		qyhous(X, i);

		uint32_t srcAddrs[] = {(uint32_t)i};
		uint32_t dstAddrs[] = {(uint32_t)(&(Q[col_index]))};
		uint32_t bufferSizes[] = {(uint32_t)EV_WINDOWS_SIZE};

		//Attendo che il DMA si disabiliti, nel caso non lo fosse
		while((streams[0]->CR & DMA_SxCR_EN) != 0);
		if(streams[0]->NDTR != 0)
				while(1);

		streams[0]->PAR = srcAddrs[0];
		streams[0]->M0AR = dstAddrs[0];
		streams[0]->NDTR = bufferSizes[0];
		/* Enable DMA Stream Transfer Complete interrupt */
		//DMA_ITConfig(streams[0], DMA_IT_TC, ENABLE);
		/* DMA Stream enable */
		/* Enable the selected DMAy Streamx by setting EN bit */
		streams[0]->CR |= DMA_SxCR_EN;

//		/* Check if the DMA Stream has been effectively enabled.
//		 * The DMA Stream Enable bit is cleared immediately by hardware if there is an
//		 * error in the configuration parameters and the transfer is no started (ie. when
//		 * wrong FIFO threshold is configured ...) */
//		while ((streams[0]->CR & DMA_SxCR_EN) == 0);

		volatile int Timeout = TIMEOUT_MAX;

		//Attendo che il DMA si disabiliti
		while ((streams[0]->CR & DMA_SxCR_EN) != 0 &&(Timeout-- > 0))
		{
			if (Timeout == 0)
			{
				while (1){}
			}
		}
		/* Reset interrupt pending bits for DMA2 Stream0 */
		DMA2->LIFCR = (uint32_t)(DMA_LISR_FEIF0 | DMA_LISR_DMEIF0 | DMA_LISR_TEIF0 | DMA_LISR_HTIF0 | DMA_LISR_TCIF0);
		/* END DMA */
	}

}

void qrhous(float32_t X[][EV_WINDOWS_SIZE], float32_t* d) {

	for(int col_index=0;col_index<N_CHANNELS; col_index++) {


		float32_t s = norm(X[col_index],col_index,EV_WINDOWS_SIZE);

		if(X[col_index][col_index]>=0) {
			d[col_index]=-s;
		}
		else {
			d[col_index]=s;
		}
		float32_t fak = sqrt(s*(s+fabs(X[col_index][col_index])));
		X[col_index][col_index] = X[col_index][col_index] - d[col_index];

		int blockSize = (int)(EV_WINDOWS_SIZE-col_index);

		arm_scale_f32(&(X[col_index][col_index]),1/fak,&(X[col_index][col_index]),blockSize);

		if(col_index<N_CHANNELS-1) {
			transform1(X, col_index);
		}
	}
}


void qyhous(float32_t X[][EV_WINDOWS_SIZE], float32_t* i) {

	for(int col_counter=N_CHANNELS-1; col_counter>=0; col_counter--) {
		transform2(X, i, col_counter);
	}
}


void transform1(float32_t X[][EV_WINDOWS_SIZE], int begin_index) {

	int new_n_rows = EV_WINDOWS_SIZE-begin_index;
	int new_n_cols = N_CHANNELS-(begin_index+1);

	float32_t v[EV_WINDOWS_SIZE] = {0};

	float32_t vvtM[N_CHANNELS][EV_WINDOWS_SIZE] = {{0}};

	/* BEGIN DMA */
	uint32_t srcAddrs[] = {(uint32_t)(((float32_t*)&(X[begin_index]))+begin_index)};
	uint32_t dstAddrs[] = {(uint32_t)(&(v[begin_index]))};
	uint32_t bufferSizes[] = {(uint32_t)new_n_rows};

	//Attendo che il DMA si disabiliti, nel caso non lo fosse
	while((streams[0]->CR & DMA_SxCR_EN) != 0);
	if(streams[0]->NDTR != 0)
			while(1);

	streams[0]->PAR = srcAddrs[0];
	streams[0]->M0AR = dstAddrs[0];
	streams[0]->NDTR = bufferSizes[0];
	/* Enable DMA Stream Transfer Complete interrupt */
	//DMA_ITConfig(streams[0], DMA_IT_TC, ENABLE);
	/* DMA Stream enable */
	/* Enable the selected DMAy Streamx by setting EN bit */
	streams[0]->CR |= DMA_SxCR_EN;

	volatile int Timeout = TIMEOUT_MAX;

	//Attendo che il DMA si disabiliti
	while ((streams[0]->CR & DMA_SxCR_EN) != 0 &&(Timeout-- > 0))
	{
		if (Timeout == 0)
		{
			while (1){}
		}
	}
	/* Reset interrupt pending bits for DMA2 Stream0 */
	DMA2->LIFCR = (uint32_t)(DMA_LISR_FEIF0 | DMA_LISR_DMEIF0 | DMA_LISR_TEIF0 | DMA_LISR_HTIF0 | DMA_LISR_TCIF0);
	/* END DMA */

	float32_t vtM[N_CHANNELS];
	mat_mult(1, EV_WINDOWS_SIZE, v, EV_WINDOWS_SIZE, N_CHANNELS, X, vtM);

	for(int i=0; i<new_n_cols; i++) {
		uint32_t address = ((uint32_t)(&(vvtM[i+begin_index+1])))+begin_index*sizeof(float32_t);
		arm_scale_f32(&(v[begin_index]), vtM[i+begin_index+1], (float32_t*)address, new_n_rows);
	}
	mat_sub(EV_WINDOWS_SIZE, N_CHANNELS, X, vvtM, X);
}


void transform2(float32_t X[][EV_WINDOWS_SIZE], float32_t* i, int begin_index) {

	int new_n_rows = EV_WINDOWS_SIZE-begin_index;
	float32_t Av[new_n_rows];
	float32_t iv[new_n_rows];

	/* BEGIN DMA */
	uint32_t srcAddrs[] = {(uint32_t)((float32_t*)(&(X[begin_index][begin_index]))),(uint32_t)((float32_t*)(&(i[begin_index])))};
	uint32_t dstAddrs[] = {(uint32_t)((float32_t*)Av),(uint32_t)((float32_t*)iv)};
	uint32_t bufferSizes[] = {(uint32_t)new_n_rows,(uint32_t)new_n_rows};

	for (int i=0; i<N_DMA_TRANSFERS; i++) {
		//Attendo che il DMA si disabiliti, nel caso non lo fosse
		while((streams[i]->CR & DMA_SxCR_EN) != 0);
		while(streams[i]->NDTR != 0);
		streams[i]->PAR = srcAddrs[i];
		streams[i]->M0AR = dstAddrs[i];
		streams[i]->NDTR = bufferSizes[i];
		/* Enable DMA Stream Transfer Complete interrupt */
		//DMA_ITConfig(streams[i], DMA_IT_TC, ENABLE);
		/* DMA Stream enable */
		/* Enable the selected DMAy Streamx by setting EN bit */
		streams[i]->CR |= DMA_SxCR_EN;
	}

	volatile int Timeout;

	for (int transferNum=0;transferNum<N_DMA_TRANSFERS;transferNum++) {
		Timeout = TIMEOUT_MAX;
		//Attendo che il DMA si disabiliti
		while ((streams[transferNum]->CR & DMA_SxCR_EN) != 0 &&(Timeout-- > 0))
		{
			if (Timeout == 0)
			{
				while (1){}
			}
		}
	}
	/* Reset interrupt pending bits for DMA2 Stream0 */
	DMA2->LIFCR = (uint32_t)(DMA_LISR_FEIF0 | DMA_LISR_DMEIF0 | DMA_LISR_TEIF0 | DMA_LISR_HTIF0 | DMA_LISR_TCIF0);
	/* Reset interrupt pending bits for DMA2 Stream1 */
	DMA2->LIFCR = (uint32_t)(DMA_LISR_FEIF0 | DMA_LISR_DMEIF0 | DMA_LISR_TEIF0 | DMA_LISR_HTIF0 | DMA_LISR_TCIF0) << 6;
	/* END DMA */


	float32_t Avtiv;
	float32_t AvAvtiv[new_n_rows];

	arm_dot_prod_f32(Av, iv, new_n_rows, &Avtiv);
	arm_scale_f32(Av, Avtiv, AvAvtiv, new_n_rows);
	arm_sub_f32(iv, AvAvtiv, iv, new_n_rows);

	//Attendo che il DMA si disabiliti, nel caso non lo fosse
	while((streams[0]->CR & DMA_SxCR_EN) != 0);
	if(streams[0]->NDTR != 0)
		while(1);

	srcAddrs[0] = (uint32_t)((float32_t*)iv);
	dstAddrs[0] = (uint32_t)((float32_t*)(&(i[begin_index])));
	streams[0]->PAR = srcAddrs[0];
	streams[0]->M0AR = dstAddrs[0];
	streams[0]->NDTR = (uint32_t)new_n_rows;
	/* DMA Stream enable */
	/* Enable the selected DMAy Streamx by setting EN bit */
	streams[0]->CR |= DMA_SxCR_EN;

	Timeout = TIMEOUT_MAX;

	//Attendo che il DMA si disabiliti
	while ((streams[0]->CR & DMA_SxCR_EN) != 0 &&(Timeout-- > 0))
	{
		if (Timeout == 0)
		{
			while (1){}
		}
	}
	/* Reset interrupt pending bits for DMA2 Stream0 */
	DMA2->LIFCR = (uint32_t)(DMA_LISR_FEIF0 | DMA_LISR_DMEIF0 | DMA_LISR_TEIF0 | DMA_LISR_HTIF0 | DMA_LISR_TCIF0);
}


void copy_samples(float32_t X[][EV_WINDOWS_SIZE], float32_t X_filtered[][SHIFT_WINDOW_SIZE/DS], ring_buffer_t* ADDdataM2)
{
	for(int i = 0; i < N_CHANNELS; i++) {
			ring_buffer_queue_arr(&ADDdataM2[i], &X_filtered[i][0], SHIFT_WINDOW_SIZE/DS);
	}
	ring_buffer_size_t items_num;
	ring_buffer_num_items(&ADDdataM2[0], &items_num);

	if(items_num < EV_WINDOWS_SIZE)
	{
		for(int ch_t = 0; ch_t<N_CHANNELS; ch_t++)
		{
			for(int i = 0; i<items_num; i++)
			{
				X[ch_t][EV_WINDOWS_SIZE-items_num+i] = ADDdataM2[ch_t].buffer[i];
			}
			center_variable(&X[ch_t][EV_WINDOWS_SIZE-items_num], items_num);
		}
	}
	else
	{
		for(int ch_t = 0; ch_t<N_CHANNELS; ch_t++)
		{
			ring_buffer_dequeue_arr(&ADDdataM2[ch_t], &X[ch_t][0], SHIFT_WINDOW_SIZE/DS);
			ring_buffer_peek_arr(&ADDdataM2[ch_t], &X[ch_t][SHIFT_WINDOW_SIZE/DS], 0, EV_WINDOWS_SIZE-(SHIFT_WINDOW_SIZE/DS));
			center_variable(X[ch_t], EV_WINDOWS_SIZE);
		}
	}
}

void downsampleData(float32_t pSrc[][SHIFT_WINDOW_SIZE], float32_t pDst[][SHIFT_WINDOW_SIZE/DS])
{
	for(int channel_index=0; channel_index<N_CHANNELS; channel_index++) {
		for (int i=0; i<SHIFT_WINDOW_SIZE/DS; i++) {
			pDst[channel_index][i] = pSrc[channel_index][i*DS];
		}
	}

}


void filterData(float32_t pSrc[][SHIFT_WINDOW_SIZE], float32_t pDst[][SHIFT_WINDOW_SIZE/DS], arm_fir_decimate_instance_f32* FIR, arm_biquad_cascade_df2T_instance_f32* IIR)
{
	float32_t pDst_tmp[N_CHANNELS][SHIFT_WINDOW_SIZE/DS];

	for(int channel_index=0;channel_index<N_CHANNELS;channel_index++)
	{
		  for(int i=0; i < FIR_NUM_BLOCKS; i++)
		  {
			  arm_fir_decimate_f32(&FIR[channel_index], (&pSrc[channel_index][0]) + (i * BLOCK_SIZE_FIR), (&pDst_tmp[channel_index][0]) + (i*BLOCK_SIZE_FIR/DS), BLOCK_SIZE_FIR);
		  }

		  for(int i=0; i < IIR_NUM_BLOCKS; i++)
		  {
			  arm_biquad_cascade_df2T_f32(&IIR[channel_index], (&pDst_tmp[channel_index][0]) + (i * BLOCK_SIZE_IIR) , (&pDst[channel_index][0]) + (i * BLOCK_SIZE_IIR), BLOCK_SIZE_IIR);
		  }
	}
}


void process_cca(float32_t X[][EV_WINDOWS_SIZE], float* correlations)
{
	// Orthogonal-triangular decomposition
	float32_t QX[N_CHANNELS][EV_WINDOWS_SIZE] = {{0}};
	qr_dec(X, QX);

	// For each frequency perform CCA on the input signal
	for (int freq_index = 0; freq_index<N_FREQs; freq_index++)
	{
		// The index of the highest value of the CORRELATIONS array identifies the index of the most
		// correlated frequency in the array TARGET_FREQ
		correlations[freq_index] = cca(freq_index, QX);
	}
}


#pragma GCC pop_options
