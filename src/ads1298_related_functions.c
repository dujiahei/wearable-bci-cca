

#include "ads1298_related_functions.h"

#define BYTES_PER_READ_DMA 27
#define USE_DMA 0

char EmptyBuffer[27];
char RXBuffer[24];

void config_ads(void)

{

					if(en_timeou >= 500)
					{

						MAIN_FSM_state = MAIN_FSM_IDLE;
						en_timeou = 0;
					}

					//------------100ms delay-------------//
					tic_config_100ms = 0;
					ms100_flag = 0;
					while(!ms100_flag)
					{

					}

					//config_value1 = 0x86 //  0.5	SPS
					//config_value1 = 0x85 //   1k	SPS
					//config_value1 = 0x84 //   2k	SPS
					//config_value1 = 0x83 //   4k	SPS
					//config_value1 = 0x82 //   8k	SPS
					//config_value1 = 0x81 //  16k	SPS
					//config_value1 = 0x80 //  32k	SPS


					//+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+
					//|R|E|G|I|S|T|E|R| |C|O|N|F|I|G|_|1|
					//+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+

					config_value1 = 0x85;
					SPI_TX_GOING = 1;
					ADS_REGISTER_WRITE_INT(CONFIG1, config_value1);

					while(SPI_TX_GOING)
					{

					}

					SPI_RX_GOING = 1;

					ADS_REGISTER_READ_INT(CONFIG1);

					while(SPI_RX_GOING)
					{

					}

					//------------100ms delay-------------//
					tic_config_100ms = 0;
					ms100_flag = 0;
					while(!ms100_flag)
					{

					}

					//------------REG CHECK-------------//
					if(SPI_RxBuffer[2] != config_value1)
					{
						while(1)
						{
							GPIO_SetBits( GPIOA, GPIO_Pin_8);  //LED BLINK
							GPIO_SetBits( GPIOA, GPIO_Pin_9);  //LED BLINK
						}
					}


					//+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+
					//|R|E|G|I|S|T|E|R| |C|O|N|F|I|G|_|2|
					//+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+

					//config_value2 = CONFIG2_INTTEST | CONFIG2_DEFAULT; // Internal test signal at default frequency (0x10)
					config_value2 = 0x12; // test other config : CONFIG2_INTTEST | CONFIG2_TESTFREQ1 (no test signal) (0x12)

					SPI_TX_GOING = 1;
					ADS_REGISTER_WRITE_INT(CONFIG2, config_value2);

					while(SPI_TX_GOING)
					{

					}

					SPI_RX_GOING = 1;

					ADS_REGISTER_READ_INT(CONFIG2);
					while(SPI_RX_GOING)
					{

					}


					//------------100ms delay-------------//
					tic_config_100ms = 0;
					ms100_flag = 0;
					while(!ms100_flag)
					{

					}

					//------------REG CHECK-------------//
					if(SPI_RxBuffer[2] != config_value2)
					{
						while(1)
						{
							GPIO_SetBits( GPIOA, GPIO_Pin_8);  //LED BLINK
							GPIO_SetBits( GPIOA, GPIO_Pin_9);  //LED BLINK
						}
					}






					//+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+
					//|R|E|G|I|S|T|E|R| |C|O|N|F|I|G|_|3|
					//+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+

					config_value3 = CONFIG3_PD_REFBUF | CONFIG3_VREF_4V|CONFIG3_RLD_STAT; //Internal ref buffer on, 4V reference, RLD disabled
					config_value3 = 0xE8; // old configuration 0xC8

					SPI_TX_GOING = 1;
					ADS_REGISTER_WRITE_INT(CONFIG3, config_value3);

					while(SPI_TX_GOING)
					{

					}

					SPI_RX_GOING = 1;

					ADS_REGISTER_READ_INT(CONFIG3);
					while(SPI_RX_GOING)
					{

					}

					//------------100ms delay-------------//
					tic_config_100ms = 0;
					ms100_flag = 0;
					while(!ms100_flag)
					{

					}

					//------------REG CHECK-------------//
					if(SPI_RxBuffer[2] != config_value3)
					{
						while(1)
						{
							GPIO_SetBits( GPIOA, GPIO_Pin_8);  //LED BLINK
							GPIO_SetBits( GPIOA, GPIO_Pin_9);  //LED BLINK
						}
					}






					//+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+
					//|L|E|A|D|_|O|F|F| |C|O|N|F|I|G|
					//+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+

					lead_off = LOFF_FLEAD_OFF1; //Lead_off disabled (0x02)
					lead_off = 0x02; // old configuration 0xC8

					SPI_TX_GOING = 1;
					ADS_REGISTER_WRITE_INT(LOFF, lead_off);

					while(SPI_TX_GOING)
					{

					}

					SPI_RX_GOING = 1;

					ADS_REGISTER_READ_INT(LOFF);
					while(SPI_RX_GOING)
					{

					}


					//------------100ms delay-------------//
					tic_config_100ms = 0;
					ms100_flag = 0;
					while(!ms100_flag)
					{

					}

					//------------REG CHECK-------------//
					if(SPI_RxBuffer[2] != lead_off)
					{
						GPIO_SetBits( GPIOA, GPIO_Pin_8);  //LED BLINK
						GPIO_SetBits( GPIOA, GPIO_Pin_9);  //LED BLINK
						while(1)
						{

						}
					}

					//+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+
					//|C|H|1|_|S|E|T| |C|O|N|F|I|G|_|1|
					//+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+

					//config_reg = CHNSET_GAIN0; // gain = 1
					//config_reg = 0x50;  //0x10 default gain = 1; 0x60 = G 12
					config_reg = 0x60;

					SPI_TX_GOING = 1;
					ADS_REGISTER_WRITE_INT(CH1SET, config_reg);

					while(SPI_TX_GOING)
					{

					}

					SPI_RX_GOING = 1;
					SPI_TxIndex = 0;
					SPI_RxIndex = 0;


					ADS_REGISTER_READ_INT(CH1SET);
					while(SPI_RX_GOING)
					{

					}


					//------------100ms delay-------------//
					tic_config_100ms = 0;
					ms100_flag = 0;
					while(!ms100_flag)
					{

					}
					//------------REG CHECK-------------//
					if(SPI_RxBuffer[2] != config_reg)
					{
						GPIO_SetBits( GPIOA, GPIO_Pin_8);  //LED BLINK
						GPIO_SetBits( GPIOA, GPIO_Pin_9);  //LED BLINK
						while(1)
						{

						}
					}



					//+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+
					//|C|H|2|_|S|E|T| |C|O|N|F|I|G|_|1|
					//+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+

					//config_reg = CHNSET_GAIN0; // gain = 1
					config_reg = 0x60;  //0x10 default gain = 1;


					SPI_TX_GOING = 1;
					ADS_REGISTER_WRITE_INT(CH2SET, config_reg);

					while(SPI_TX_GOING)
					{

					}

					SPI_RX_GOING = 1;
					SPI_TxIndex = 0;
					SPI_RxIndex = 0;


					ADS_REGISTER_READ_INT(CH2SET);
					while(SPI_RX_GOING)
					{

					}

					//------------100ms delay-------------//
					tic_config_100ms = 0;
					ms100_flag = 0;
					while(!ms100_flag)
					{

					}

					//------------REG CHECK-------------//
					if(SPI_RxBuffer[2] != config_reg)
					{
						while(1)
						{
							GPIO_SetBits( GPIOA, GPIO_Pin_8);  //LED BLINK
							GPIO_SetBits( GPIOA, GPIO_Pin_9);  //LED BLINK
						}
					}




					//+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+
					//|C|H|3|_|S|E|T| |C|O|N|F|I|G|_|1|
					//+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+

					//config_reg = CHNSET_GAIN0; // gain = 1
					config_reg = 0x60; //0x10 default gain = 1;


					SPI_TX_GOING = 1;
					ADS_REGISTER_WRITE_INT(CH3SET, config_reg);

					while(SPI_TX_GOING)
					{

					}

					SPI_RX_GOING = 1;
					SPI_TxIndex = 0;
					SPI_RxIndex = 0;


					ADS_REGISTER_READ_INT(CH3SET);
					while(SPI_RX_GOING)
					{

					}

					//------------100ms delay-------------//
					tic_config_100ms = 0;
					ms100_flag = 0;
					while(!ms100_flag)
					{

					}

					//------------REG CHECK-------------//
					if(SPI_RxBuffer[2] != config_reg)
					{
						GPIO_SetBits( GPIOA, GPIO_Pin_8);  //LED BLINK
						GPIO_SetBits( GPIOA, GPIO_Pin_9);  //LED BLINK
						while(1)
						{

						}
					}




					//+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+
					//|C|H|4|_|S|E|T| |C|O|N|F|I|G|_|1|
					//+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+

					//config_reg = CHNSET_GAIN0; // gain = 1
					config_reg = 0x60;  //0x10 default gain = 1;


					SPI_TX_GOING = 1;
					ADS_REGISTER_WRITE_INT(CH4SET, config_reg);

					while(SPI_TX_GOING)
					{

					}

					SPI_RX_GOING = 1;
					SPI_TxIndex = 0;
					SPI_RxIndex = 0;


					ADS_REGISTER_READ_INT(CH4SET);
					while(SPI_RX_GOING)
					{

					}

					//------------100ms delay-------------//
					tic_config_100ms = 0;
					ms100_flag = 0;
					while(!ms100_flag)
					{

					}

					//------------REG CHECK-------------//
					if(SPI_RxBuffer[2] != config_reg)
					{
						GPIO_SetBits( GPIOA, GPIO_Pin_8);  //LED BLINK
						GPIO_SetBits( GPIOA, GPIO_Pin_9);  //LED BLINK
						while(1)
						{

						}
					}




					//+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+
					//|C|H|5|_|S|E|T| |C|O|N|F|I|G|_|1|
					//+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+

					//config_reg = CHNSET_GAIN0; // gain = 1
					config_reg = 0x60;  //0x10 default gain = 1;


					SPI_TX_GOING = 1;
					ADS_REGISTER_WRITE_INT(CH5SET, config_reg);

					while(SPI_TX_GOING)
					{

					}

					SPI_RX_GOING = 1;
					SPI_TxIndex = 0;
					SPI_RxIndex = 0;


					ADS_REGISTER_READ_INT(CH5SET);
					while(SPI_RX_GOING)
					{

					}

					//------------100ms delay-------------//
					tic_config_100ms = 0;
					ms100_flag = 0;
					while(!ms100_flag)
					{

					}

					//------------REG CHECK-------------//
					if(SPI_RxBuffer[2] != config_reg)
					{
						GPIO_SetBits( GPIOA, GPIO_Pin_8);  //LED BLINK
						GPIO_SetBits( GPIOA, GPIO_Pin_9);  //LED BLINK
						while(1)
						{

						}
					}




					//+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+
					//|C|H|6|_|S|E|T| |C|O|N|F|I|G|_|1|
					//+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+
					//config_reg = CHNSET_GAIN0; // gain = 1
					config_reg = 0x60;  //0x10 default gain = 1;


					SPI_TX_GOING = 1;
					ADS_REGISTER_WRITE_INT(CH6SET, config_reg);

					while(SPI_TX_GOING)
					{

					}

					SPI_RX_GOING = 1;
					SPI_TxIndex = 0;
					SPI_RxIndex = 0;


					ADS_REGISTER_READ_INT(CH6SET);
					while(SPI_RX_GOING)
					{

					}

					//------------100ms delay-------------//
					tic_config_100ms = 0;
					ms100_flag = 0;
					while(!ms100_flag)
					{

					}

					//------------REG CHECK-------------//
					if(SPI_RxBuffer[2] != config_reg)
					{
						GPIO_SetBits( GPIOA, GPIO_Pin_8);  //LED BLINK
						GPIO_SetBits( GPIOA, GPIO_Pin_9);  //LED BLINK
						while(1)
						{

						}
					}




					//+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+
					//|C|H|7|_|S|E|T| |C|O|N|F|I|G|_|1|
					//+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+
					//config_reg = CHNSET_GAIN0; // gain = 1
					config_reg = 0x60;  //0x10 default gain = 1;


					SPI_TX_GOING = 1;
					ADS_REGISTER_WRITE_INT(CH7SET, config_reg);

					while(SPI_TX_GOING)
					{

					}

					SPI_RX_GOING = 1;
					SPI_TxIndex = 0;
					SPI_RxIndex = 0;


					ADS_REGISTER_READ_INT(CH7SET);
					while(SPI_RX_GOING)
					{

					}

					//------------100ms delay-------------//
					tic_config_100ms = 0;
					ms100_flag = 0;
					while(!ms100_flag)
					{

					}

					//------------REG CHECK-------------//
					if(SPI_RxBuffer[2] != config_reg)
					{
						GPIO_SetBits( GPIOA, GPIO_Pin_8);  //LED BLINK
						GPIO_SetBits( GPIOA, GPIO_Pin_9);  //LED BLINK
//						while(1)
//						{
//
//						}
					}



					//+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+
					//|C|H|8|_|S|E|T| |C|O|N|F|I|G|_|1|
					//+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+
					//config_reg = CHNSET_GAIN0; // gain = 1
					config_reg = 0x60;  //0x10 default gain = 1;


					SPI_TX_GOING = 1;
					ADS_REGISTER_WRITE_INT(CH8SET, config_reg);

					while(SPI_TX_GOING)
					{

					}

					SPI_RX_GOING = 1;
					SPI_TxIndex = 0;
					SPI_RxIndex = 0;


					ADS_REGISTER_READ_INT(CH8SET);
					while(SPI_RX_GOING)
					{

					}

					//------------100ms delay-------------//
					tic_config_100ms = 0;
					ms100_flag = 0;
					while(!ms100_flag)
					{

					}

					//------------REG CHECK-------------//
					if(SPI_RxBuffer[2] != config_reg)
					{
						GPIO_SetBits( GPIOA, GPIO_Pin_8);  //LED BLINK
						GPIO_SetBits( GPIOA, GPIO_Pin_9);  //LED BLINK
//						while(1)
//						{

//						}
					}

					MAIN_FSM_state = MAIN_FSM_IDLE;
}



void ADS_Config_CONTROL_PINS(void)
{
 	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
 	GPIO_InitTypeDef GPIO_InitStructure;


    /* Configure the CLKSEL PIN 0 FOR EXTERNAL CLOCK  (HW P_DN)  */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOE, &GPIO_InitStructure);

    GPIO_ResetBits(GPIOE, GPIO_Pin_12);   //SET EXTERNAL ADS CLOCK


    /* Configure the DAISY_IN PIN  0 FOR NORMAL OPERATIONS */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOE, &GPIO_InitStructure);

	 GPIO_ResetBits(GPIOE, GPIO_Pin_11);   //SET DAISY_IN PIN

    /* Configure the PWDN PIN    1 POWER ON , 0 POWER OFF   (HW P_UP)*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOE, &GPIO_InitStructure);

	 GPIO_ResetBits(GPIOE, GPIO_Pin_10);   //TURN OFF ADS


	/* Configure the RESET PIN      0 RESET STATE  (HW P_UP) */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOE, &GPIO_InitStructure);

	 GPIO_ResetBits(GPIOE, GPIO_Pin_9);   //PUT ADS IN RESET STATE

	    /* Configure the START PIN     1 FOR START  (HW P_UP) */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOE, &GPIO_InitStructure);

	 GPIO_ResetBits(GPIOE, GPIO_Pin_8);   //SET START LOW

}

void ADS_SPI_Config(void)
{

	GPIO_InitTypeDef GPIO_InitStructure;
	SPI_InitTypeDef SPI_InitStructure;

	/* Enable the GPIO BUS */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

    /* Enable the SPI BUS */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);

/* SET CLOCK, MOSI, MISO pins as Alternate Function for SPI2, check if i can optimize it*/

	GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_SPI2);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource15, GPIO_AF_SPI2);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource14, GPIO_AF_SPI2);

  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;

  	/* SPI CLK pin configuration*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
  	GPIO_Init(GPIOB, &GPIO_InitStructure);

	/* SPI Data line MOSI init*/
 	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
  	GPIO_Init(GPIOB, &GPIO_InitStructure);


  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
   	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
   	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
   	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;

	/* SPI Data line MISO init*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

    /* SPI Enable CS not controlled by the peripheral*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
  	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_Init(GPIOB, &GPIO_InitStructure);

	SPI_I2S_DeInit(SPI2); // not sure that it is in the right place

	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;

  	//SPI_InitStructure.SPI_Direction = SPI_Direction_1Line_Tx; /* One wire Tx*/
  	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low; /* Clock polarity*/
  	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;/* */
  	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
  	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  	SPI_InitStructure.SPI_CRCPolynomial = 7;

  	SPI_Init(SPI2, &SPI_InitStructure);

  	SPI_Cmd(SPI2, ENABLE);

  	//SPI_ReceiveData(SPI2);

}

void ADS_SPI_Config_DMA(void)
{

	GPIO_InitTypeDef GPIO_InitStructure;
	SPI_InitTypeDef SPI_InitStructure;
	DMA_InitTypeDef       DMA_InitStructure;
	NVIC_InitTypeDef NVIC_InitStruct;

	/* Enable the GPIO BUS */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

    /* Enable the SPI BUS */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);

/* SET CLOCK, MOSI, MISO pins as Alternate Function for SPI2, check if i can optimize it*/

	GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_SPI2);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource15, GPIO_AF_SPI2);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource14, GPIO_AF_SPI2);

  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;

  	/* SPI CLK pin configuration*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
  	GPIO_Init(GPIOB, &GPIO_InitStructure);

	/* SPI Data line MOSI init*/
 	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
  	GPIO_Init(GPIOB, &GPIO_InitStructure);


  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
   	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
   	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
   	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;

	/* SPI Data line MISO init*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

    /* SPI Enable CS not controlled by the peripheral*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
  	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_Init(GPIOB, &GPIO_InitStructure);

	SPI_I2S_DeInit(SPI2); // not sure that it is in the right place

	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;

  	//SPI_InitStructure.SPI_Direction = SPI_Direction_1Line_Tx; /* One wire Tx*/
  	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low; /* Clock polarity*/
  	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;/* */
  	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
  	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  	SPI_InitStructure.SPI_CRCPolynomial = 7;

  	SPI_Init(SPI2, &SPI_InitStructure);

    while(SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET);

  	/* Enable DMA1 clock */
  		RCC->AHB1ENR |= RCC_AHB1ENR_DMA1EN;

  		DMA_Cmd(DMA1_Stream3, DISABLE);
  		DMA_DeInit(DMA1_Stream3); //SPI1_RX_DMA_STREAM
  		DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable ;
  		DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull ;
  	    DMA_InitStructure.DMA_BufferSize = (uint32_t)BYTES_PER_READ_DMA;
  		DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single ;
  		DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  		DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  		DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
  		DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&(SPI2->DR));
  		DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
  		DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
  	    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  		DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  		DMA_InitStructure.DMA_Channel = DMA_Channel_0;
  	    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
  	    DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)(SPI_RxBuffer);

  	  DMA_Init(DMA1_Stream3, &DMA_InitStructure);

  	  DMA_Cmd(DMA1_Stream4, DISABLE);
  	  DMA_DeInit(DMA1_Stream4); //SPI1_TX_DMA_STREAM

  	  DMA_InitStructure.DMA_Channel = DMA_Channel_0;
  	  DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
  	  DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)EmptyBuffer;
  	  DMA_Init(DMA1_Stream4, &DMA_InitStructure);

  	  // Enable the SPI Rx/Tx DMA request
  	  SPI_I2S_DMACmd(SPI2, SPI_I2S_DMAReq_Rx, ENABLE);
  	  SPI_I2S_DMACmd(SPI2, SPI_I2S_DMAReq_Tx, ENABLE);

      /* Enable global interrupts for DMA stream */
      NVIC_InitStruct.NVIC_IRQChannel = DMA1_Stream3_IRQn;
      NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
      NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
      NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
      NVIC_Init(&NVIC_InitStruct);

  	  DMA_ITConfig(DMA1_Stream3, DMA_IT_TC, ENABLE);

  	  SPI_Cmd(SPI2, ENABLE);
}

void ADS_REGISTER_WRITE_INT(char reg_addr, char reg_value)
{

	ADS_SEND_SDATAC();

    SPI_TxBuffer[0] = WREG | reg_addr;
    SPI_TxBuffer[1] = 0x00; //number of registers to write -1;
    SPI_TxBuffer[2] = reg_value; //number of registers to write -1;

    SPI_TxIndex = 0;
	SPI_I2S_ITConfig(SPI2, SPI_I2S_IT_TXE, ENABLE);
}

void ADS_REGISTER_READ_INT(char reg_addr)
{

	 ADS_SEND_SDATAC();

    SPI_TxBuffer[0] = RREG | reg_addr;
    SPI_TxBuffer[1] = 0x00; //number of registers to read -1;
    SPI_TxBuffer[2] = 0x00; //0x00 read ;

    SPI_TxIndex = 0;
    SPI_RxIndex = 0;

    SPI_I2S_ReceiveData(SPI2);  //clear the receiver pending flag...
    SPI_I2S_ITConfig(SPI2, SPI_I2S_IT_RXNE, ENABLE);
	SPI_I2S_ITConfig(SPI2, SPI_I2S_IT_TXE, ENABLE);
}

char ADS_REGISTER_WRITE(char reg_addr, char reg_value)
{

	char opcode1;
	char opcode2;


	SPI_SendData(SPI2,SDATAC);	//send first byte (SDATAC)

	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET))
	{}
	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_BSY) == SET))
	{}

	opcode1 = WREG | reg_addr;
	opcode2 = 0x00; //number of registers to read -1;


	SPI_SendData(SPI2,opcode1);	//send first byte (op_code_1)

	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET))
	{}
	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_BSY) == SET))
	{}



	SPI_SendData(SPI2,opcode2);	//send second byte (op_code_2)

	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET))
	{}
	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_BSY) == SET))
	{}


	/*!< Loop while DR register in not emplty */


	SPI_SendData(SPI2,reg_value);	//send third byte value

	while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET)
	{}


	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_BSY) == SET))
	{}




	return 0;

}

char ADS_REGISTER_READ(char reg_in)
{
	char value;
	char opcode1;
	char opcode2;

	SPI_SendData(SPI2,SDATAC);	//send first byte (SDATAC)

	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET))
	{}
	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_BSY) == SET))
	{}


	opcode1 = RREG | reg_in;
	opcode2 = 0x00; //number of registers to read -1;


	SPI_SendData(SPI2,opcode1);	//send first byte (op_code_1)

	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET))
	{}
	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_BSY) == SET))
	{}



	SPI_SendData(SPI2,opcode2);	//send second byte (op_code_2)

	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET))
	{}
	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_BSY) == SET))
	{}

	/** flush away any rogue data in rx buffer **/
	if (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_RXNE) == SET)
	{
		SPI_I2S_ReceiveData(SPI2);
	}

	/*!< Loop while DR register in not emplty */


	SPI_SendData(SPI2,0x00);	//send third byte 0x00

	while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET)
	{}


	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_BSY) == SET))
	{}

	/*!< Wait to receive a byte */
	while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_RXNE) == RESET);

	value = SPI_ReceiveData(SPI2);


	return value;
}

void ADS_SEND_SDATAC(void)
{
	SPI_SendData(SPI2,SDATAC);	//send first byte (SDATAC)

	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET))
	{}
	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_BSY) == SET))
	{}

}

void ADS_SEND_RDATAC(void)
{
	SPI_SendData(SPI2,RDATAC);	//send first byte (SDATAC)

	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET))
	{}
	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_BSY) == SET))
	{}

}

void ADS_SEND_RDATA(void)
{
	SPI_SendData(SPI2,RDATA);	//send first byte (SDATAC)

	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET))
	{}
	while ((SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_BSY) == SET))
	{}

}
extern int dma_working;

void ADS_Read_DMA(){
	while(((DMA2_Stream0->CR)&(DMA_SxCR_EN))==DMA_SxCR_EN);

			DMA_ClearITPendingBit(DMA1_Stream4,DMA_IT_HTIF4);
			DMA_ClearITPendingBit(DMA1_Stream4,DMA_IT_TCIF4);
		    DMA_ClearITPendingBit(DMA1_Stream3,DMA_IT_HTIF3);
			DMA_ClearITPendingBit(DMA1_Stream3,DMA_IT_TCIF3);

			DMA1_Stream3->NDTR=(uint32_t)BYTES_PER_READ_DMA;
			//DMA2_Stream0->M0AR=(uint32_t)(DMA2_Stream0->M0AR+BYTES_PER_READ);
			DMA1_Stream4->NDTR=(uint32_t)BYTES_PER_READ_DMA;
			DMA1_Stream3->CR|=(uint32_t)DMA_SxCR_EN;
			DMA1_Stream4->CR|=(uint32_t)DMA_SxCR_EN;
			dma_working = 1;

			//PWR_EnterSTOPMode(PWR_Regulator_ON,PWR_STOPEntry_WFI); //GO TO SLEEP
	}
