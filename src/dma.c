/*
 * dma.c
 *
 *  Created on: 30 Jul 2018
 *      Author: matti
 */

/**
  ******************************************************************************
  * @file    main.c
  * @author  Simone Benatti & Bojan Milosevic
  * @version V1.0
  * @date    27-Apr-2017
  * @brief   DMA example
  ******************************************************************************
*/


/*********** Includes  ****************/

#include "stm32f4_discovery.h"
#include "defines.h"

#include "dma.h"


/*********** Defines  *****************
#define BUFFER_SIZE              32
#define TIMEOUT_MAX              1000 /* Maximum timeout value */



/*********** Functions *****************/


/**
  * @brief  Configure the DMA controller according to the Stream parameters
  * @param  None
  * @retval None
  */
void DMA_Config(DMA_Stream_TypeDef* stream[], uint32_t channel[], uint32_t incPer[], uint32_t incMem[])
{
	DMA_InitTypeDef  DMA_InitStructure[N_DMA_TRANSFERS];
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Enable DMA clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);

	for(int transferNum=0;transferNum<N_DMA_TRANSFERS;transferNum++) {

	/* Reset DMA Stream registers (for debug purpose) */
	DMA_DeInit(stream[transferNum]);

	/* Check if the DMA Stream is disabled before enabling it.
	 Note that this step is useful when the same Stream is used multiple times:
	 enabled, then disabled then re-enabled... In this case, the DMA Stream disable
	 will be effective only at the end of the ongoing data transfer and it will
	 not be possible to re-configure it before making sure that the Enable bit
	 has been cleared by hardware. If the Stream is used only once, this step might
	 be bypassed. */
	while (DMA_GetCmdStatus(stream[transferNum]) != DISABLE){}

	/* Configure DMA Stream */
	DMA_InitStructure[transferNum].DMA_Channel = channel[transferNum];
	DMA_InitStructure[transferNum].DMA_DIR = DMA_DIR_MemoryToMemory;
	DMA_InitStructure[transferNum].DMA_BufferSize = 0;
	if (incPer[transferNum])
		DMA_InitStructure[transferNum].DMA_PeripheralInc = DMA_PeripheralInc_Enable;
	else
		DMA_InitStructure[transferNum].DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	if(incMem[transferNum])
		DMA_InitStructure[transferNum].DMA_MemoryInc = DMA_MemoryInc_Enable;
	else
		DMA_InitStructure[transferNum].DMA_MemoryInc = DMA_MemoryInc_Disable;
	DMA_InitStructure[transferNum].DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;
	DMA_InitStructure[transferNum].DMA_MemoryDataSize = DMA_MemoryDataSize_Word;
	DMA_InitStructure[transferNum].DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure[transferNum].DMA_Priority = DMA_Priority_High;
	DMA_InitStructure[transferNum].DMA_FIFOMode = DMA_FIFOMode_Enable;
	DMA_InitStructure[transferNum].DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
	DMA_InitStructure[transferNum].DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure[transferNum].DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(stream[transferNum], &(DMA_InitStructure[transferNum]));

	}

	/* Configure DMA NVIC */
	NVIC_InitStructure.NVIC_IRQChannel = DMA2_Stream0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

}

/**
  * @brief  Compares two buffers.
  * @param  pBuffer, pBuffer1: buffers to be compared.
  * @param  BufferLength: buffer's length
  * @retval PASSED: pBuffer identical to pBuffer1
  *         FAILED: pBuffer differs from pBuffer1
  */
TestStatus Buffercmp(uint32_t* pBuffer, uint32_t* pBuffer1, uint16_t bufferLength)
{
  while(bufferLength--)
  {
    if(*pBuffer != *pBuffer1)
    {
      return FAILED;
    }

    pBuffer++;
    pBuffer1++;
  }

  return PASSED;
}
