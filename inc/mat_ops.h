/*
 * mat_ops.h
 *
 *  Created on: 23 Apr 2018
 *      Author: matti
 */

#ifndef MAT_OPS_H_
#define MAT_OPS_H_

#include "defines_cca.h"
#include "arm_math.h"

void mat_mult_t1_tres(int n_rows_m1, int n_cols_m1, float mat_1[][n_rows_m1], int n_rows_m2, int n_cols_m2, float mat_2[][n_rows_m2], float mat_res[][n_cols_m2]);
void mat_mult_t1(int n_rows_m1, int n_cols_m1, float mat_1[][n_rows_m1],int n_rows_m2, int n_cols_m2, float mat_2[][n_rows_m2], float mat_res[][n_cols_m1]);
void mat_mult(int n_rows_m1, int n_cols_m1, float32_t mat_1[][n_rows_m1],int n_rows_m2, int n_cols_m2, float32_t mat_2[][n_rows_m2], float32_t mat_res[][n_rows_m1]);
void mat_sub(int n_rows, int n_cols, float a[][n_rows], float b[][n_rows], float r[][n_rows]);
float norm(float* x, int begin_index, int end_index);
void center_variable(float* X, int length);

#endif /* MAT_OPS_H_ */
