/*
 * dma.h
 *
 *  Created on: 30 Jul 2018
 *      Author: matti
 */

#ifndef DMA_H_
#define DMA_H_

#include "stm32f4xx.h"
#define N_DMA_TRANSFERS 2
#define TIMEOUT_MAX 1000

typedef enum {FAILED = 0, PASSED = !FAILED} TestStatus;

void DMA_Config(DMA_Stream_TypeDef* stream[], uint32_t channel[], uint32_t incPer[], uint32_t incMem[]);
TestStatus Buffercmp(uint32_t* pBuffer, uint32_t* pBuffer1, uint16_t bufferLength);
#endif /* DMA_H_ */
