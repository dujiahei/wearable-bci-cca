#ifndef HANDLERS_H_
#define HANDLERS_H_

#include "stm32f4xx.h"
#include "defines.h"
#include "defines_cca.h"

extern int CommTxBufferIndex;
extern char CommTxBuffer[COMM_TX_BUFFER_SIZE];
extern int current_TX_buffer_length;
extern int tic_config_100ms;
extern int tic_config_500ms;
extern int ms100_flag;
extern int ms500_flag;
extern int en_timeou;
extern int ms10_flag;
extern int SPI_RX_GOING;
extern char SPI_Rx_maxV;
extern char SPI_RxBuffer[SPI_BUFFER_MAXSIZE];
extern uint8_t SPI_RxIndex;
extern int SPI_TX_GOING;
extern char SPI_Tx_maxV;
extern char SPI_TxBuffer[SPI_BUFFER_MAXSIZE];
extern uint8_t SPI_TxIndex;
extern int stream_enable;
extern uint32_t valore;
extern MAIN_FSM MAIN_FSM_state;
extern int new_window_available;
extern int send_packet;




void DMA2_Stream0_IRQHandler(void);
void EXTI9_5_IRQHandler(void);
void USART2_IRQHandler(void);
void SysTick_Handler(void);



#endif /* HANDLERS_H_ */
