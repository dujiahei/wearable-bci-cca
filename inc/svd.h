/*
 * svd.h
 *
 *  Created on: 20 Nov 2017
 *      Author: matti
 */

#ifndef SVD_H_
#define SVD_H_

int dsvd(int m, int n, float a[][n], float *w, float v[][n]);


#endif /* SVD_H_ */
