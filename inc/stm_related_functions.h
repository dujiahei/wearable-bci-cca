#ifndef STM_RELATED_FUNCTIONS_H_
#define STM_RELATED_FUNCTIONS_H_

#include "stm32f4xx.h"
#include "defines.h"
#include "defines_cca.h"
#include "arm_math.h"

#define MF 100


extern int32_t ADDdata[8];
extern char CommTxBuffer[COMM_TX_BUFFER_SIZE];
extern int current_TX_buffer_length;
extern char packetcounter;
extern int pkt_idx;
extern char SPI_RxBuffer[SPI_BUFFER_MAXSIZE];
extern uint32_t valore;


void GPIO_Config_LEDS(void);
void GPIO_Config_REGULATOR_DRIVERS(void);
void SPI_DRDY_EXTI_Config(void);
void SPI_NVIC_Config(void);
void USART_Config(void);
void USART_Prepare_Packet(void);
extern void delay_500ms();

int32_t two2dec(uint32_t val);
extern CURRENT_MATRIX MATRIX_SELECTOR;
extern float reception_matrix_A[N_CHANNELS][SHIFT_WINDOW_SIZE];
extern float reception_matrix_B[N_CHANNELS][SHIFT_WINDOW_SIZE];
extern int micro_window_counter;
extern int temp_downsample;
extern int new_window_available;
extern float correlations[N_FREQs];
extern int send_correlations;
extern int current_class;
extern int32_t trigger;
extern float SVM_output;

extern float32_t ADDdataM[N_CHANNELS][SHIFT_WINDOW_SIZE];

#endif /* STM_RELATED_FUNCTIONS_H_ */
