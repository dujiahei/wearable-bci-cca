/*
 * cca.h
 *
 *  Created on: 15 Nov 2017
 *      Author: matti
 */

#ifndef CCA_H_
#define CCA_H_



#include "defines_cca.h"
#include "arm_math.h"
#include "ringbuffer.h"


float32_t cca(int freq_index, float32_t QX[][EV_WINDOWS_SIZE]);
void qr_dec(float32_t X[N_CHANNELS][EV_WINDOWS_SIZE], float32_t Q[N_CHANNELS][EV_WINDOWS_SIZE]);
void qrhous(float32_t X[][EV_WINDOWS_SIZE], float32_t* d);
void qyhous(float32_t X[][EV_WINDOWS_SIZE], float32_t* i);
void transform1(float32_t X[][EV_WINDOWS_SIZE], int begin_index);
void transform2(float32_t X[][EV_WINDOWS_SIZE], float32_t* i, int begin_index);

void copy_samples(float32_t X[][EV_WINDOWS_SIZE], float32_t X_filtered[][SHIFT_WINDOW_SIZE/DS], ring_buffer_t* ADDdataM2);
void downsampleData(float32_t pSrc[][SHIFT_WINDOW_SIZE], float32_t pDst[][SHIFT_WINDOW_SIZE/DS]);
void filterData(float32_t pSrc[][SHIFT_WINDOW_SIZE], float32_t pDst[][SHIFT_WINDOW_SIZE/DS], arm_fir_decimate_instance_f32* FIR, arm_biquad_cascade_df2T_instance_f32* IIR);
void process_cca(float32_t X[][EV_WINDOWS_SIZE], float* correlations);

#endif /* CCA_H_ */
