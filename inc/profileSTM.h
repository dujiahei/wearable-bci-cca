/*
 * profile_stm.h
 *
 *  Created on: 23 Apr 2018
 *      Author: matti
 */

#ifndef PROFILE_STM_H_
#define PROFILE_STM_H_

unsigned int cyc[2];
volatile unsigned int *DWT_CYCCNT;
volatile unsigned int *DWT_CONTROL;
volatile unsigned int *SCB_DEMCR;

#define STOPWATCH_START {cyc[0]=*DWT_CYCCNT;} 								// start counting
#define STOPWATCH_STOP  {cyc[1]=*DWT_CYCCNT; cyc[1]=cyc[1]-cyc[0];}			// stop counting, result is in cyc[1]


#endif /* PROFILE_STM_H_ */
